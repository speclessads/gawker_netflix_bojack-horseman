#Specless Cascade
###Ad Details
When beginning a Cascade project update the ad details contained in "_ad-details.kit", this will allow the ad to be generated with necessary meta information for use within Specless.

###HTML
Edit files within the html directory
* Add banner content to "_banner.html"
* Add panel (expanded state) content to "_panel.html"
* The HTML structure is handled upon generation of the final ad file
* Only the body of the creative HTML is needed along with any external link and/or script sources
* External asset source paths should be relative to top directory, "/img/file_name.jpg"


###SASS

####Mixins
#####Flowlanes
#####Breakpoints

###Images

###JS

###Fonts
